// Facade

export class CommentHandler {
  constructor() {
    this._messageBlock = document.querySelector(".message-wrapper");
    this._commentsQueue = [];
    this._isCommentShowing = false;
    this.TIME_SHOWING_MESSAGE = 3;
  }

  showComment = (comment) => {
    this._addCommentToQueue(comment);
    this._checkCommentShowing();
  };

  _addCommentToQueue = (comment) => this._commentsQueue.unshift(comment);

  _checkCommentShowing = () => {
    if (!this._isCommentShowing) {
      this._isCommentShowing = true;
      this._showLastCommentFromQueue();
    }
  };

  _showLastCommentFromQueue = () => {
    const lastComment = this._commentsQueue[this._commentsQueue.length - 1];
    if (!lastComment) {
      this._isCommentShowing = false;
      this._messageBlock.innerText = "";
      return;
    } else {
      this._messageBlock.innerText = lastComment;
      this._commentsQueue.pop();
      setTimeout(this._showLastCommentFromQueue, 3 * 1000);
    }
  };
}
