import { addClass, removeClass, createElement } from "./helper.mjs";
import { showModal } from "./modal.mjs";

export class GameHandler {
  constructor(socket, userName) {
    this.socket = socket;
    this.userName = userName;
    this.roomsContainer = document.querySelector(".rooms-container");

    this.readyButton = document.getElementById("ready-btn");
    this.readyButton.addEventListener("click", () => {
      if (this.readyButton.classList.contains("ready")) {
        removeClass(this.readyButton, "ready");
        addClass(this.readyButton, "notready");
      } else {
        removeClass(this.readyButton, "notready");
        addClass(this.readyButton, "ready");
      }
      this.socket.emit("READY");
    });

    this.backToRoomsButton = document.getElementById("quit-room-btn");
    this.backToRoomsButton.addEventListener("click", () => {
      removeClass(this.roomsPage, "display-none");
      addClass(this.gamePage, "display-none");
      if (this.readyButton.classList.contains("notready")) {
        removeClass(this.readyButton, "notready");
        addClass(this.readyButton, "ready");
      }
      this.socket.emit("LEAVE_ROOM");
    });
  }

  createUserCard = ({ name, isReady, progress }) => {
    const card = createElement({
      tagName: "li",
      className: "user",
    });

    card.innerHTML = `
    <div class="user-progress-text-wrapper">
      <div class="${isReady ? "ready-status-green" : "ready-status-red"}"></div>
      <span ${this.userName === name ? 'class="active"' : ""}>
      ${name}
      </span>
    </div>
    <div class="user-progress-wrapper">
      <div class="user-progress ${name}${
      progress === 100 ? " finished" : ""
    }" style="width: ${progress}%;"></div>
    </div>
    `;

    return card;
  };

  onUpdatePlayRoom = (room) => {
    this.gamePage = document.getElementById("game-page");
    if (this.gamePage.classList.contains("display-none")) {
      removeClass(this.gamePage, "display-none");
      this.roomsPage = document.getElementById("rooms-page");
      addClass(this.roomsPage, "display-none");
    }
    this.showRoom(room);
  };

  showRoom = ({ name, users }) => {
    const roomName = this.gamePage.querySelector("h1");
    roomName.innerText = name;
    this.roomName = name;

    const usersCard = users.map(this.createUserCard);
    this.usersList = this.gamePage.querySelector(".users-container");
    this.usersList.innerHTML = "";
    this.usersList.append(...usersCard);
  };

  onStartTimer = ({ time, textId, gameTime }) => {
    addClass(this.backToRoomsButton, "display-none");
    addClass(this.readyButton, "display-none");
    this.timer = document.getElementById("timer");
    removeClass(this.timer, "display-none");

    let text;

    this.getText(textId)
      .then((newText) => (text = newText))
      .catch(console.log);

    const timerId = setInterval(() => {
      this.timer.innerText = time--;
      if (time === 0) {
        this.startGame({ gameTime, text });
        clearInterval(timerId);
        this.timer.innerText = "";
      }
    }, 1000);
  };

  startGame = ({ gameTime, text }) => {
    this.onKeyDown = (event) => {
      if (event.key === this.currentLetter.innerText) {
        this.currentLetterIndex++;
        this.doneText.innerText = this.letters
          .slice(0, this.currentLetterIndex)
          .join("");
        this.currentLetter.innerText =
          this.letters[this.currentLetterIndex] || "";
        this.nextText.innerText = this.letters
          .slice(this.currentLetterIndex + 1)
          .join("");

        if (this.currentLetterIndex === this.letters.length)
          addClass(this.progresBar, "finished");
        const newProgress =
          (this.currentLetterIndex / this.letters.length) * 100;
        this.socket.emit("PROGRESS_CHANGED", newProgress);
      }
    };

    window.addEventListener("keydown", this.onKeyDown);

    addClass(this.timer, "display-none");

    this.textContainer = document.getElementById("text-container");
    this.doneText = this.textContainer.querySelector(".done");
    this.currentLetter = this.textContainer.querySelector(".current");
    this.nextText = this.textContainer.querySelector(".next");

    this.letters = text.split("");
    this.currentLetterIndex = 0;

    this.currentLetter.innerText = text[this.currentLetterIndex];
    this.nextText.innerText = this.letters.slice(1).join("");

    this.progresBar = document.querySelector(`.user-progress.${this.userName}`);

    removeClass(this.textContainer, "display-none");

    this.gameTimer = document.querySelector(".game-timer");

    this.timerId = setInterval(() => {
      this.gameTimer.innerText = gameTime--;
      if (gameTime === -1) {
        clearInterval(this.timerId);
      }
    }, 1000);
  };

  getText = async (textId) => {
    const res = await fetch(`/game/texts/${textId}`);
    const text = await res.json();

    return text;
  };

  onShowModal = (results) => {
    const ratingList = createElement({ tagName: "ol", className: "results" });
    const userItems = results.map((username, ind) => {
      const item = createElement({ tagName: "li" });
      item.id = `place-${ind + 1}`;
      item.innerText = username;
      return item;
    });

    ratingList.append(...userItems);
    showModal({
      title: "Results",
      bodyElement: ratingList,
    });
  };

  onResults = (results) => {
    clearInterval(this.timerId);
    window.removeEventListener("keydown", this.onKeyDown);
    this.doneText.innerText = "";
    this.currentLetter.innerText = "";
    this.nextText.innerText = "";
    this.gameTimer.innerText = "";
    removeClass(this.readyButton, "display-none");
    removeClass(this.backToRoomsButton, "display-none");
    addClass(this.textContainer, "display-none");
    removeClass(this.readyButton, "notready");
    addClass(this.readyButton, "ready");
  };
}
