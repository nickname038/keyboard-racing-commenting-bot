import { createElement } from "./helper.mjs";

export class RoomsHandler {
  constructor(socket) {
    this.socket = socket;
    this.roomsContainer = document.querySelector(".rooms-container");

    const addRoomButton = document.getElementById("add-room-btn");
    addRoomButton.addEventListener("click", () => {
      const roomName = prompt("Enter the name of the room");
      if (roomName) {
        socket.emit("CREATE_ROOM", roomName);
      }
    });
  }

  onAuthorisationError = () => {
    alert("User with the same name already exists");
    sessionStorage.clear();
    window.location.replace("/login");
    this.socket.disconnect();
  };

  onUpdateRooms = rooms => {
    const allRooms = rooms.map(this.createRoom);
    this.roomsContainer.innerHTML = "";
    this.roomsContainer.append(...allRooms);
  };

  onCreateRoomError = () => {
    alert("Error: Room with the same name already exists");
  };

  createRoom = ({ name, userCount }) => {
    const room = createElement({
      tagName: "li",
      className: "room",
    });

    room.innerHTML = `
    <span>${userCount} user${userCount === 1 ? "" : "s"} connected</span>
    <h2>${name}</h2>
    <button class="join-btn">Join</button>
    `;

    const joinButton = room.querySelector(".join-btn");
    joinButton.addEventListener("click", () => {
      this.socket.emit("JOIN_ROOM", name);
    });

    return room;
  };
}
