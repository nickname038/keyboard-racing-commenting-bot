import {
  transports,
  selfPresentation,
  presentationOfParticipants,
  checkStatusText,
  places,
  approachingFinishLine,
  approachingFinishPoint,
  finishLine,
  finishLinePoint,
  endGame,
  factsIntro,
  facts,
} from "./ComentatorData";

export class Comentator {
  constructor() {
    this.approachingFinishPoint = approachingFinishPoint;
    this.finishLinePoint = finishLinePoint;
  }

  checkProgress = ({ users, timeToEnd }) => {
    let text = checkStatusText[0];
    text += `${timeToEnd} cекунд.`;
    text += checkStatusText[1];
    const rating = users.sort((user1, user2) => user2.progress - user1.progress);

    for (let i = 0; i < 3; i++) {
      if (!rating[i]) break;
      text += places[i];
      text += rating[i].name;
    }

    return text;
  };

  selfPresent = () => {
    return selfPresentation;
  };

  participantsPresent = () => {
    let text = presentationOfParticipants;
    for (const participant of this.participants.values()) {
      text += ` ${participant.name} ${participant.transport}`;
    }
    return text;
  };

  setParticipants = (names) => {
    this.participants = new Map();

    names.forEach((name) => {
      this.participants.set(name, {
        name,
        transport: this.getNewTransport(),
      });
    });
  };

  getNewTransport = () => {
    const index = this.randomInteger(0, transports.length - 1);
    const transport = transports[index];
    return transport;
  };

  randomInteger = (min, max) => {
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
  };

  onApproachingFinishLine = (username) => {
    let text = approachingFinishLine;
    text += username;
    return text;
  };

  onFinishLine = (username) => {
    let text = finishLine;
    text += username;
    return text;
  };

  onEndGame = ({ results, currentGameDuration }) => {
    let text = endGame[0];
    for (let i = 0; i < 3; i++) {
      if (!results[i]) break;
      text += places[i];
      text += results[i].name;
    }
    text += endGame[1];
    text += `${currentGameDuration - 10} секунд.`;
    return text;
  };

  getFact = () => {
    let text = factsIntro;
    const factIndex = this.randomInteger(0, facts.length - 1);
    text += facts[factIndex];
    return text;
  };
}
