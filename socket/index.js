import { RoomLogicHandler } from "./roomsLogic";
import { Comentator } from "./Commentator";
const _ = require("lodash");

const userNames = new Set();
export default (io) => {
  io.of("").on("connection", (socket) => {
    const commentator = new Comentator();
    const commentatorEmitWrapper = (io, fn, room, parametr) =>
      io.to(room).emit("COMMENT", fn(parametr));
    const commentatorEmitWrapperToIo = _.partial(commentatorEmitWrapper, io);

    const checkProgress = _.partial(
      commentatorEmitWrapperToIo,
      commentator.checkProgress
    );

    const username = socket.handshake.query.username;
    const roomLogicHandler = new RoomLogicHandler(
      io,
      socket,
      username,
      checkProgress
    );

    if (userNames.has(username)) {
      socket.emit("AUTHORISATION_ERROR");
    } else {
      userNames.add(username);
      socket.emit("UPDATE_ROOMS", roomLogicHandler.getRooms());
    }

    socket.on("CREATE_ROOM", roomLogicHandler.onCreateRoom);
    socket.on("JOIN_ROOM", roomLogicHandler.onJoinRoom);

    const getFact = _.partial(commentatorEmitWrapperToIo, commentator.getFact);
    const selfPresent = _.partial(
      commentatorEmitWrapperToIo,
      commentator.selfPresent
    );
    const participantsPresent = _.partial(
      commentatorEmitWrapperToIo,
      commentator.participantsPresent
    );
    const onEndGame = _.partial(
      commentatorEmitWrapperToIo,
      commentator.onEndGame
    );
    const onReady = _.partial(roomLogicHandler.onReady, {
      selfPresent,
      setParticipants: commentator.setParticipants,
      participantsPresent,
      getFact,
      onEndGame,
    });
    socket.on("READY", onReady);

    const onLeave = _.partial(roomLogicHandler.onLeaveRoom, {
      selfPresent,
      setParticipants: commentator.setParticipants,
      participantsPresent,
      onEndGame,
      getFact,
    });
    socket.on("LEAVE_ROOM", onLeave);

    const onApproachingFinishLine = _.partial(
      commentatorEmitWrapperToIo,
      commentator.onApproachingFinishLine
    );
    const onFinishLine = _.partial(
      commentatorEmitWrapperToIo,
      commentator.onFinishLine
    );
    const onProgressChange = _.partial(
      roomLogicHandler.onProgressChange,
      {
        onApproachingFinishLine,
        approachingFinishPoint: commentator.approachingFinishPoint,
      },
      {
        onFinishLine,
        finishLinePoint: commentator.finishLinePoint,
      },
      onEndGame
    );
    socket.on("PROGRESS_CHANGED", onProgressChange);

    socket.on("DELETE_USER", (username) => {
      userNames.delete(username);
    });

    socket.on("disconnect", roomLogicHandler.onLeaveRoom);
  });
};
