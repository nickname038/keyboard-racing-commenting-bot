// Factory Pattern

class Room {
  create(type, name) {
    let room;
    if (type === "gameRoom") {
      room = new GameRoom(name);
    }

    room.type = type;

    return room;
  }
}

class GameRoom {
  constructor(name) {
    this.name = name;
    this.isAvailable = true;
    this.isGameInProgress = false;
    this.users = [];
  }
}

export const factory = new Room();
