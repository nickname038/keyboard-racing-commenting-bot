import * as config from "./config";
import { texts } from "./../data";
import { factory } from "./roomFactory";

const rooms = new Map();

const newUser = username => ({
  name: username,
  isReady: false,
  progress: 0
});

export class RoomLogicHandler {
  constructor(io, socket, username, checkProgress) {
    this.io = io;
    this.socket = socket;
    this.username = username;
    this.room = null;
    this.results = [];
    this.checkProgress = checkProgress;

    this.CHECK_TIME_SECONDS = 30;
    this.FACT_INTERVAL_TIME = 30000;
  }

  getRooms = () => {
    const allRooms = Array.from(rooms.values());
    const availableRooms = allRooms.filter((room) => room.isAvailable);
    return availableRooms.map((room) => ({
      ...room,
      userCount: room.userCount,
    }));
  };

  onCreateRoom = (newRoomName) => {
    if (!rooms.has(newRoomName)) {
      let newRoom = factory.create("gameRoom", newRoomName);
      // Proxy
      newRoom = new Proxy(newRoom, {
        get(target, prop) {
          if (prop === "userCount") {
            return target.users.length;
          } else {
            return target[prop];
          }
        },
      });

      newRoom.users.push(newUser(this.username));
      rooms.set(newRoomName, newRoom);
      this.room = newRoom;
      this.socket.broadcast.emit("UPDATE_ROOMS", this.getRooms());
      this.socket.join(newRoomName);
      this.socket.emit("UPDATE_PLAY_ROOM", this.room);
    } else {
      this.socket.emit("CREATE_ROOM_ERROR");
    }
  };

  onJoinRoom = (roomName) => {
    this.socket.join(roomName);
    const currentRoomState = rooms.get(roomName);

    currentRoomState.users.push(newUser(this.username));

    this.room = currentRoomState;
    this.io.to(roomName).emit("UPDATE_PLAY_ROOM", this.room);
    if (this.room.userCount >= config.MAXIMUM_USERS_FOR_ONE_ROOM) {
      this.room.isAvailable = false;
      rooms.set(roomName, this.room);
    }
    this.socket.broadcast.emit("UPDATE_ROOMS", this.getRooms());
  };

  onLeaveRoom = (callbacksObj) => {
    if (this.room) {
      const { name: roomName } = this.room;
      this.socket.leave(roomName);
      const userObj = this.getUserByName(this.username);
      const index = this.room.users.indexOf(userObj);
      this.room.users.splice(index, 1);
      if (!this.room.isGameInProgress) {
        this.room.isAvailable = true;
      }
      rooms.set(roomName, this.room);
      if (this.room.userCount <= 0) {
        rooms.delete(roomName);
      } else {
        if (!this.room.isGameInProgress) {
          this.checkAllUsersReady(callbacksObj);
        }
      }

      if (this.room.isGameInProgress) {
        this.checkAllUsersFinished(callbacksObj.onEndGame);
      }

      this.io.to(roomName).emit("UPDATE_PLAY_ROOM", this.room);
      this.io.of("").emit("UPDATE_ROOMS", this.getRooms());
    }
  };

  getUserByName = (name) => {
    let searchedUser;
    for (const user of this.room.users) {
      if (user.name === name) {
        searchedUser = user;
        break;
      }
    }
    return searchedUser;
  };

  checkAllUsersFinished = (onEndGame) => {
    if (
      this.room.users[0] &&
      this.room.users.every((user) => user.progress === 100)
    ) {
      this.onEndGame(onEndGame);
      clearTimeout(this.timerId);
    }
  };

  getRandomTextId = () => {
    const textListSize = texts.length - 1;
    const index = Math.floor(Math.random() * (textListSize + 1));
    this.textSize = texts[index].length;
    return index;
  };

  onReady = (callbacksObj) => {
    const { name: roomName } = this.room;
    const userObj = this.getUserByName(this.username);
    userObj.isReady = !userObj.isReady;
    if (userObj.isReady) {
      this.checkAllUsersReady(callbacksObj);
    }
    this.io.to(roomName).emit("UPDATE_PLAY_ROOM", this.room);
  };

  checkAllUsersReady = ({
    setParticipants,
    selfPresent,
    participantsPresent,
    getFact,
    onEndGame,
  }) => {
    if (this.room.users.every((user) => user.isReady)) {
      const textId = this.getRandomTextId();
      this.room.isGameInProgress = true;
      this.room.isAvailable = false;
      this.socket.broadcast.emit("UPDATE_ROOMS", this.getRooms());
      this.io.to(this.room.name).emit("START_TIMER", {
        time: config.SECONDS_TIMER_BEFORE_START_GAME,
        textId,
        gameTime: config.SECONDS_FOR_GAME,
      });

      const gameDuration =
        (config.SECONDS_TIMER_BEFORE_START_GAME + config.SECONDS_FOR_GAME) *
        1000;

      this.gameStartTime = Date.now();

      this.timerId = setTimeout(() => {
        this.onEndGame(onEndGame);
      }, gameDuration);

      const factIntervalFunction = () => {
        getFact(this.room.name);
        this.factTimerId = setTimeout(
          factIntervalFunction,
          this.FACT_INTERVAL_TIME
        );
      };
      this.factTimerId = setInterval(
        factIntervalFunction,
        this.FACT_INTERVAL_TIME
      );

      setTimeout(() => {
        let timeout = 0;
        this.checktimerId = setInterval(() => {
          timeout += this.CHECK_TIME_SECONDS;
          if (timeout >= config.SECONDS_FOR_GAME) {
            clearInterval(this.checktimerId);
          }
          const timeToEnd = config.SECONDS_FOR_GAME - timeout;
          this.checkProgress(this.room.name, {
            users: this.room.users,
            timeToEnd,
          });
        }, this.CHECK_TIME_SECONDS * 1000);
      }, config.SECONDS_TIMER_BEFORE_START_GAME * 1000);

      selfPresent(this.room.name);
      setParticipants(this.room.users.map((user) => user.name));
      participantsPresent(this.room.name);
    }
  };

  onProgressChange = (
    { onApproachingFinishLine, approachingFinishPoint },
    { onFinishLine, finishLinePoint },
    onEndGame,
    progress
  ) => {
    const userObj = this.getUserByName(this.username);
    userObj.progress = progress;

    if (progress === 100) {
      userObj.timeFinished = Date.now();
      this.checkAllUsersFinished(onEndGame);
    } else if (
      progress.toFixed(2) ===
      ((approachingFinishPoint * 100) / this.textSize).toFixed(2)
    ) {
      onApproachingFinishLine(this.room.name, userObj.name);
    } else if (
      progress.toFixed(2) ===
      ((finishLinePoint * 100) / this.textSize).toFixed(2)
    ) {
      onFinishLine(this.room.name, userObj.name);
    }

    this.io.to(this.room.name).emit("UPDATE_PLAY_ROOM", this.room);
  }; 

  onEndGame = (showResults) => {
    const finishedUsers = [];
    const notFinishedUsers = [];

    clearInterval(this.checktimerId);
    clearInterval(this.factTimerId);

    this.room.users.forEach((user) => {
      if (user.timeFinished) {
        finishedUsers.push(user);
      } else {
        notFinishedUsers.push(user);
      }
    });

    finishedUsers.sort(
      (user1, user2) => user1.timeFinished - user2.timeFinished
    );
    notFinishedUsers.sort((user1, user2) => user2.progress - user1.progress);

    const results = finishedUsers.concat(notFinishedUsers);

    const currentGameDuration = (Date.now() - this.gameStartTime) / 1000;
    showResults(this.room.name, { results, currentGameDuration });

    this.io.to(this.room.name).emit(
      "RESULTS",
      results.map((user) => user.name)
    );

    this.room.users.forEach((user) => {
      user.isReady = false;
      user.progress = 0;
      user.timeFinished = undefined;
    });

    this.io.to(this.room.name).emit("UPDATE_PLAY_ROOM", this.room);

    this.room.isGameInProgress = false;
    if (this.room.userCount < config.MAXIMUM_USERS_FOR_ONE_ROOM) {
      this.room.isAvailable = true;
    }

    this.io.of("").emit("UPDATE_ROOMS", this.getRooms());
  };
}
